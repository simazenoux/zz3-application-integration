import logging
from os import environ
from random import random
from time import sleep

from celery import Celery
from celery.exceptions import Retry, Reject
from ortools.constraint_solver.routing_enums_pb2 import FirstSolutionStrategy
from ortools.constraint_solver.pywrapcp import RoutingIndexManager, RoutingModel, DefaultRoutingSearchParameters

CELERY_BROKER_URL=environ['CELERY_BROKER_URL']
CELERY_RESULT_BACKEND=environ['CELERY_RESULT_BACKEND']

celery = Celery('tasks',
                broker=CELERY_BROKER_URL,
                backend=CELERY_RESULT_BACKEND)


@celery.task(bind=True, name='tasks.add')
def add(self, x: int, y: int):
    try:
        sleep(10)
        if random() <= 0.1:
            raise Exception("Unlucky")
        return x + y
    except Exception as e:
        self.retry(exc=e, countdown=1)
    

@celery.task(bind=True, name='tasks.divide', acts_late=True)
def divide(self, a: int, b: int):
    try:
        sleep(10)
        return a/b
    except ZeroDivisionError as e:
        raise Reject(e)
        # Doesn't work, check https://github.com/celery/celery/issues/4222
    except Exception as e:
        self.retry(exc=e)



@celery.task(name='tasks.task_a', bind=True, acts_late=True)
def task_a(self, s: str):
    sleep(3)
    logging.info("Job received in task A")
    if random() <= 0.1:
        self.retry()
        logging.info("Need retry")
    # next = Operations().getValue("taskA/nextTask", "tasks.task_b")
    celery.send_task("tasks.task_b", [s])
    


@celery.task(name='tasks.task_b', bind=True, acts_late=True)
def task_b(self, s: str):
    sleep(3)
    logging.info("Job received in task B")
    if random() <= 0.1:
        self.retry()
        logging.info("Need retry")
    celery.send_task("tasks.task_c", [s])


@celery.task(name='tasks.task_c', bind=True, acts_late=True)
def task_c(self, s: str):
    sleep(3)
    logging.info("Job received in task C")
    if random() <= 0.1:
        celery.send_task("tasks.task_a", [s])
    else:
        print("END")


def tsp_bis(self, data):
    pass
    


@celery.task(name='tasks.tsp', bind=True, acts_late=True)
def tsp(self, data):

    # Create the routing index manager.
    manager = RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = RoutingModel(manager)

    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    print(transit_callback_index)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Setting first solution heuristic.
    search_parameters = DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = FirstSolutionStrategy.PATH_CHEAPEST_ARC

    # Sets a time limit of 10 seconds.
    routing.parameters.max_time_in_seconds = 10.0


    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    def print_solution(manager, routing, solution):
        """Prints solution on console."""
        print('Objective: {} miles'.format(solution.ObjectiveValue()))
        index = routing.Start(0)
        plan_output = 'Route for vehicle 0:\n'
        route_distance = 0
        while not routing.IsEnd(index):
            plan_output += ' {} ->'.format(manager.IndexToNode(index))
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
        plan_output += ' {}\n'.format(manager.IndexToNode(index))
        print(plan_output)
        plan_output += 'Route distance: {}miles\n'.format(route_distance)


    if solution:
        print_solution(manager, routing, solution)



